module Proxy where

import Data.Conduit
import Data.Conduit.Binary    ( sinkLbs )
import Control.Monad.IO.Class ( liftIO )
import qualified Snap.Core                  as S
import qualified Network.HTTP.Conduit       as C
import qualified Data.ByteString.Char8      as B
import qualified Data.ByteString.Lazy.Char8 as L

proxyReq :: String -> S.Request -> S.Snap ()
proxyReq url req = do
    reqBody <- S.readRequestBody 1073741824 -- 1gibibyte!
    liftIO $ putStrLn $ "Proxying to " ++ url
    body <- liftIO $ runResourceT $ do
        manager <- liftIO $ C.newManager C.def
        cReq    <- liftIO $ C.parseUrl url
        let cReq' = transferRequest reqBody req cReq
        res     <- C.http cReq' manager
        C.responseBody res $$+- sinkLbs
    S.writeBS $ L.toStrict body

transferRequest :: L.ByteString -> S.Request -> C.Request m -> C.Request m
transferRequest body req cReq = cReq { C.method = B.pack $ show $ S.rqMethod req
                                     , C.checkStatus = \_ _ _ -> Nothing
                                     , C.requestBody = C.RequestBodyLBS body
                                     }

