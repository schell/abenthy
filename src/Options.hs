module Options where

import Data.Monoid
import System.Console.GetOpt
import Snap.Core
import Snap.Http.Server

data UserConfig = UserConfig { routes :: [String]
                             , urls   :: [String]
                             } deriving (Show, Eq)

instance Monoid UserConfig where
    mempty = UserConfig [] []
    mappend a b = a { routes = routes a ++ routes b
                    , urls   = urls a ++ urls b
                    }

type SnapConfig = Config Snap UserConfig

type RouteProxyPair = (String,String)

getConfig :: IO (SnapConfig, [RouteProxyPair])
getConfig = do
    scfg <- snapConfig emptyConfig
    let mcfg = fmap (\c -> zip (routes c) (urls c)) $ getOther scfg
    return $ maybe (scfg,[]) ((,) scfg) mcfg

snapConfig :: SnapConfig -> IO SnapConfig
snapConfig defaults = extendedCommandLineConfig (userOptions ++ optDescrs defaults) mappend defaults


userOptions :: [OptDescr (Maybe SnapConfig)]
userOptions = map (fmapOpt $ fmap (`setOther` mempty))
    [ Option "r" ["route"]
        (ReqArg setr "ROUTE")
        "A local route to serve proxy requests from."
    , Option "u" ["proxy_url"]
        (ReqArg setp "PROXY_URL")
        "A remote url to proxy requests to."
    ]
        where setr s = Just $ mempty { routes = [s] }
              setp s = Just $ mempty { urls = [s] }

