{-# LANGUAGE OverloadedStrings #-}
module Main where

import Options
import Proxy
import Control.Monad
import Data.Maybe
import Data.List
import Snap.Core
import Snap.Http.Server

import qualified Data.ByteString.Char8 as B



main :: IO ()
main = do
    (cfg, pairs) <- getConfig
    if null pairs
      then putStrLn "There must be at least one route and one proxy url."
      else httpServe cfg $ proxyTo pairs 

proxyTo :: [RouteProxyPair] -> Snap ()
proxyTo pairs = do
    uri <- fmap B.unpack $ getsRequest rqURI
    req <- getRequest
    let mRemoteUri = lookupRoute uri pairs
    unless (isNothing mRemoteUri) $ proxyReq (fromJust mRemoteUri) req 

lookupRoute :: String -> [RouteProxyPair] -> Maybe String
lookupRoute uri = foldl getRemote Nothing
    where getRemote mR (r,u) = if isNothing mR && r `isPrefixOf` uri
                                 then Just $ u ++ drop (length r) uri
                                 else mR

